module RaceBet
  class Race

    class << self
      def score(guesses, winners)

        score = 0
        winner_scores = [15, 10, 5, 3, 1]

        guesses.each_with_index do |guess, i|
          if guess == winners[i] # the guess is correct
            score += winner_scores[i]
          elsif i < 5 && winners[0..4].include?(guess) # the guess is among 5 winners, but misplaced
            score += 1
          end
        end

        score
      end
    end

  end
end
